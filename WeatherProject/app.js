//jshint esversion:6
const express = require("express");
const https = require("https");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.urlencoded({extended: true}));

app.get("/", function(req, res){
  res.sendFile(__dirname + "/index.html");
});

app.post("/", function(req, res){
  const query = req.body.cityName;
  const apiKey = "41ad33ba59dcf2fc99c337183fba796f";
  const url = "https://api.openweathermap.org/data/2.5/weather?q=" + query +"&appid=" +apiKey;
  https.get(url, function(res){
    console.log(res);
    res.on("data", function(data){
      console.log(data);
      const weatherData = JSON.parse(data);
      const temp = weatherData.main.temp;
      const weatherDescription = weatherData.weather[0].description;
      const icon = weatherData.weather[0].icon;
      const imageUrl = " http://openweathermap.org/img/wn/" + icon + "10d@2x.png";
      res.write("<p>The weather is currently " + weatherDescription + "</p>");
      res.write("<h1>The temperature in " + query + " is " + temp + " degree Celcius.</h1>");
      res.write("<img src=" + imageUrl +">");
      res.send();
    });
  });
});

app.listen(3000, function(){
  console.log("Sever is running on port 3000");
});
